from . import models
from rest_framework import viewsets
from rest_framework import mixins


class OrderViewset(
    mixins.CreateModelMixin,
    viewsets.GenericViewSet,
):
    serializer_class = models.OrderSerializer
