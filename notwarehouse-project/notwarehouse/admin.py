from django.contrib.admin import register
from django.contrib.admin import ModelAdmin

from notwarehouse import models


@register(models.Order)
class OrderAdmin(ModelAdmin):
    readonly_fields = 'code', 'comment'

    def has_add_permission(self, request):
        return False
