from urllib.parse import urljoin

import requests as rq
from rest_framework import serializers
from rest_framework import status
from notorder import models

from django.conf import settings

UPDATE_ACTION = 2


class Order(models.Order):
    """Order model

    :param state: state of an order.
    :param comment: comment a user leaves.
    :param code: cross service unique identifier of order>
    """
    def save(self, *args, **kwargs):
        pk = self.pk
        super().save(*args, **kwargs)
        if pk:
            self._notify_observer()

    def _notify_observer(self):
        """it could be a celery async task."""
        serializer = OrderSerializer(self)
        res = rq.post(
            urljoin(settings.ORDER_DISPATCHER_ADDRESS, 'notify/update-order/'),
            json={
                'action': UPDATE_ACTION,
                'data': str(serializer.data),
            },
        )
        assert res.status_code == status.HTTP_201_CREATED,\
            f'{res.status_code}: {res.text}'


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        exclude = 'id',
        model = Order
