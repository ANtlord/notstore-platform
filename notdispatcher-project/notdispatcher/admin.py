from django.contrib.admin import register
from django.contrib.admin import ModelAdmin

from . import models


@register(models.OrderNotication)
class OrderNoticationAdmin(ModelAdmin):
    pass
