from urllib.parse import urljoin

import requests as rq

from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers
from rest_framework import status
from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.decorators import list_route

from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

from . import models


UUID_FIELD = 'code'


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OrderNotication
        fields = '__all__'

    def validate(self, data):
        order_data = eval(data['data'])
        if UUID_FIELD not in order_data:
            raise serializers.ValidationError('Order data must contain uuid')
        return data


def get_order_create_subscribers():
    items = settings.SUBSCRIBERS.items()
    return (v for _, v in items if 'order_create' in v['events'])


def get_order_update_subscribers():
    items = settings.SUBSCRIBERS.items()
    return (v for _, v in items if 'order_update' in v['events'])


def notify_create_subscribers(serialized_order: dict):
    """It could a celery task."""
    for subscriber in get_order_create_subscribers():
        res = rq.post(
            urljoin(subscriber['address'], 'order/'),
            json=serialized_order
        )
        assert res.status_code == status.HTTP_201_CREATED,\
            f'{res.status_code}: {res.text}'


def notify_update_subscribers(serialized_order: dict, order_uuid: str):
    """It could a celery task."""
    for subscriber in get_order_update_subscribers():
        res = rq.put(
            urljoin(subscriber['address'], f'order/{order_uuid}/'),
            json=serialized_order
        )
        assert res.status_code == status.HTTP_200_OK,\
            f'{res.status_code}: {res.text}'


class NotificationViewset(viewsets.GenericViewSet):
    serializer_class = NotificationSerializer

    def _create_notification(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer

    @csrf_exempt
    @list_route(methods=['POST'], url_path='create-order')
    def create_order(self, request, *args, **kwargs):
        serializer = self._create_notification(request)
        serialized_order = eval(serializer.data['data'])
        notify_create_subscribers(serialized_order)
        return Response(serializer.data, status.HTTP_201_CREATED)

    @csrf_exempt
    @list_route(methods=['POST'], url_path='update-order')
    def update_order(self, request, *args, **kwargs):
        serializer = self._create_notification(request)
        serialized_order = eval(serializer.data['data'])
        uuid_code = serialized_order[UUID_FIELD]
        notify_update_subscribers(serialized_order, uuid_code)
        return Response(serializer.data, status.HTTP_201_CREATED)
