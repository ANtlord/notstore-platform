from django.db import models
from django.utils.translation import ugettext_lazy as _


class OrderNotication(models.Model):
    """Represents the notification subscribers get"""
    CREATE = 1
    UPDATE = 2

    ACTIONS = (
        (CREATE, _('Create')),
        (UPDATE, _('Update')),
    )

    action = models.PositiveSmallIntegerField(choices=ACTIONS)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    # In production I use json field and Postgres. For the assignment I don't
    # want deploy postgres.
    data = models.TextField(blank=False)

    class Meta:
        verbose_name = _('notification')
        verbose_name_plural = _('notifications')
