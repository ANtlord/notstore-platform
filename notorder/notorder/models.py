import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Order(models.Model):
    """Order model

    :param state: state of an order.
    :param comment: comment a user leaves.
    :param code: cross service unique identifier of order>
    """
    NEW = 1
    IN_PROGRESS = 2
    DONE = 3

    STATES = (
        (NEW, _('New')),
        (IN_PROGRESS, _('In progress')),
        (DONE, _('Done')),
    )

    state = models.PositiveIntegerField(choices=STATES, default=NEW)
    comment = models.TextField(blank=True)
    code = models.UUIDField(unique=True, default=uuid.uuid4)

    class Meta:
        abstract = True
        verbose_name = _('order')
        verbose_name_plural = _('orders')
