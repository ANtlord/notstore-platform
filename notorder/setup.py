from setuptools import setup, find_packages

setup(
    name='Not An Order',
    version='0.0.1',
    packages=['notorder'],
    url='',
    license='',
    author='',
    author_email='',
    description='',
    setup_requires=[],
    install_requires=[
        'Django==2.0.4',
    ],
    tests_require=[],
    include_package_data=True,
)
