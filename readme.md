# Not A Store Platoform

Test assignment for Alexey Markov

## Abstract

Objective is create two django web applications Store and Warehouse implementing synchronization of orders
in databases of the applications.

## Repository description

There are 4 packages. 3 of them are web applications and 1 of them is common django application contains base.

Web applications:
* The first web application is Store allows creating an order.
* The second web application is Warehouse allows changing the status an order has.
* The third web application is Dispatcher controls data flow between the Store and the Warehouse.

The Store and The Warehouse are about their purposes consider the assignement description.
The Dispatcher make binding between the two applications weaker working as mediator.

Separated django application called *notorder* provides abstract model Order as a common of the Store and
the Warehouse.

## Installation of dependencies

All web applications in the repository provide the same dependency set so in order to install them

* checkout `notstore-project` folder
* run `pip install -e .`

Installation of *notorder* package could be done by next steps

* checkout `notorder` folder
* run `pip install -e .`

## Create initial data

In order to setup databases for the projects you need to run migrations one by one for every project
in the repository. Then setup super users to get an ability working on orders.

## Run all applications

The repository consists of three web applications so in order to run them properly install
honcho `pip install honcho` and run `honcho start` keeping root folder of the repository.
