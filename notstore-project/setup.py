from setuptools import setup, find_packages

setup(
    name='Not A Store',
    version='0.0.1',
    packages=['notstore'],
    url='',
    license='',
    author='',
    author_email='',
    description='',
    setup_requires=['pytest-runner', 'flake8'],
    install_requires=[
        'Django==2.0.4',
        # 'psycopg2==2.7.3.1',
        'djangorestframework==3.8.2',
        'requests==2.18.4',
    ],
    tests_require=[],
    include_package_data=True,
)
