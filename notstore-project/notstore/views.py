from rest_framework import viewsets
from rest_framework import mixins

from . import models


class OrderViewset(
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    queryset = models.Order.objects.all()
    lookup_field = 'code'
    serializer_class = models.OrderSerializer
