from django.contrib.admin import register
from django.contrib.admin import ModelAdmin

from notstore import models


@register(models.Order)
class OrderAdmin(ModelAdmin):
    readonly_fields = 'state', 'code'
